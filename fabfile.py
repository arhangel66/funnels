# -*- coding:utf-8 -*-
import datetime

from fabric.api import env, sudo, cd
from fabric.decorators import task

try:
    from fabfile_config_local import PASSWORD
except ImportError:
    pass
else:
    env.password = PASSWORD

#env.user = 'ubuntu'
DEFAULT_HOST = '52.6.72.82'
env.user = 'ubuntu'
env.key_filename = '~/.ssh/GTG_05032016.pem'

env.roledefs = {
    'all': [DEFAULT_HOST, ],
    'default': [DEFAULT_HOST],
}

CONFIG = {
    DEFAULT_HOST: {
        'sources_folder': '/home/django/projects/funnels',
        'interpreter': '/home/django/.virtualenvs/funnels/bin/python',
        # 'celery_tasks': 'celery-mystatsonline',
    },
}
"""
sudo su - django
cd /home/django/projects/funnels
. /home/django/.virtualenvs/funnels/bin/activate
"""

@task
def deploy():
    env.settings = CONFIG[env['host']]
    print(45, env.settings)
    with cd(env.settings['sources_folder']):
        sudo('git pull', user='django')
        # sudo('pip install -r requirements/production.txt', user='django')
        sudo('%(interpreter)s manage.py migrate --noinput --settings=config.settings.production' % env.settings, user='django')
        sudo('%(interpreter)s manage.py collectstatic --noinput --settings=config.settings.production' % env.settings, user='django')

        sudo('touch uwsgi.ini', user='django')

        print(datetime.datetime.now())
