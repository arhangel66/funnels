from collections import Counter
from pprint import pprint


__author__ = 'Derbichev Mikhail, arhangel662@gmail.com'
import os

from django.core.wsgi import get_wsgi_application

os.environ['DJANGO_SETTINGS_MODULE'] = 'apps.diet.settings'
application = get_wsgi_application()
from apps.limelight.models import CrmLogs, Order

# for log in CrmLogs.objects.all():
#     print(log.create_order())
from apps.main.cart import Cart
from django.conf import settings
cart = Cart()
# cart.add_to_cart(settings.PRODUCTS['3'])
# cart.add_to_cart(settings.PRODUCTS['upsell'])
# cart.add_to_cart(settings.PRODUCTS['0'])

pprint(cart.products)
print(cart.calc_total())
print(cart.calc_total_price())
print(cart.calc_total_shipping())
# from apps.limelight.tasks import *
# print(test_task.delay())

# print(CrmLogs.objects.get(id=2).get_error_text())

# from diet.confi
# import datetime
# from apps.limelight.tasks import test_task
# print(datetime.datetime.now(), test_task.delay())


from apps.limelight.tasks import call
# log = CrmLogs.objects.get(id=24)
# print(log.create_order())
# order = Order.objects.get(id=20)
# addr = order.get_address_dict()
# pprint(addr)
# dict = {'CVV': '123',
#         'billingAddress1': '',
#         'billingCity': '',
#         'billingCountry': '',
#         'billingSameAsShipping': 'yes',
#         'billingState': '',
#         'billingZip': '',
#         'campaignId': '148',
#         'creditCardNumber': '4640111111111111',
#         'creditCardType': 'visa',
#         'csrfmiddlewaretoken': 'SL49whb5wSaQPty5xtB8w81MCAi5SOpC',
#         'dynamic_product_price_270': '29',
#         'email': 'arhangel662@gmail.com',
#         'expirationDate': '0521',
#         'expmonth': '05',
#         'expyear': '21',
#         'firstName': 'test',
#         'ipAddress': '127.0.0.1',
#         'lastName': 'test',
#         'limelight_charset': 'utf-8',
#         'method': 'NewOrder',
#         'phone': '9090706997',
#         'productId': '270',
#         'productPrice': '149.95',
#         'product_qty_270': '5',
#         'shippingAddress1': 'ssadf 123 sdf',
#         'shippingCity': 'test',
#         'shippingCountry': 'US',
#         'shippingId': '1',
#         'shippingState': 'AZ',
#         'shippingZip': '12312',
#         'tranType': 'Sale',
#         'username': 'totalgarcinia'}
# from apps.limelight.tasks import call
# call(dict)
text = """
<div id="checkout_banner">
                <center><img src="images/step_one.jpg" class="full_img"></center>
                <div class="form_heading">
                    <img src="images/off_icon.png" class="off_icon">
                    <span class="front_text">Your</span>
                    <span class="off_text">80<sup>%</sup></span>
                    <span class="green_text">Discount<br><span>Has Been Applied</span></span>
                </div>
                <div class="shipping_box">
                    <img src="images/us_postal_service.jpg">
                    <p>
                        Your order is scheduled to arrive by
                        <span>
                            <script type="text/javascript">getDate(5);</script>
                        </span>
                    </p>
                </div>
                <div class="step_heading">
                    <img src="images/stack_icon.png">
                    <span>Step #1:</span> Select Quantity
                </div>
                <div class="best_offer">
                    <input type="radio" name="packSelect" id="BestSeller" data-pid="1" data-val="39" checked="" data-name="Buy 3 VR Shinecon Headsets,GET 2 FREE!" data-each="29.99" data-qty="5" data-price="149.95" class="radio_button">
                    <div class="pack_type">
                        <label for="BestSeller"><strong><span>BEST SELLER!</span><br>
                        Buy 3 VR Shinecon Headsets,<br> GET 2 FREE! ($149.95 Total)</strong></label>
                    </div>
                    <div class="price_box">
                        <strong>$29/ea</strong>
                    </div>
                </div>
                <div class="pack_details">
                    <input type="radio" name="packSelect" id="set3" data-qty="3" data-pid="2" data-val="44" data-name="Buy 2 VR Shinecon Headsets, GET 1 FREE!" data-each="39.99" data-price="119.97" class="radio_button_common">
                    <div class="pack_type">
                        <label for="set3">Buy 2 VR Shinecon Headsets,<br> GET 1 FREE! ($119.97 Total)</label>
                    </div>
                    <div class="price_box">
                        $39/ea
                    </div>
                </div>
                <div class="pack_details">
                    <input type="radio" name="packSelect" id="set1" data-pid="3" data-val="45" data-name="Buy 1 VR Shinecon Headset, GET 1 FREE!" data-each="59.99" data-price="59.99" data-qty="1" class="radio_button_common">
                    <div class="pack_type">
                        <label for="set1">Buy 1 VR Shinecon Headset<br> ($59 Per Unit)</label>
                    </div>
                    <div class="price_box">
                        $59/ea
                    </div>
                </div>
                <div class="pack_details">
                    <input type="radio" name="packSelect" id="set2" data-qty="2" data-pid="4" data-val="46" data-name="Buy 2 VR Shinecon Headsets" data-each="48.50" data-price="97.00" class="radio_button_common">
                    <div class="pack_type">
                        <label for="set2">Buy 2 VR Shinecon Headsets<br> ($48.50 Per Unit)</label>
                    </div>
                    <div class="price_box">
                        $97
                    </div>
                </div>
                <div class="pack_details">
                    <input type="radio" name="packSelect" id="set4" data-qty="4" data-pid="5" data-val="47" data-name="Buy 4 VR Shinecon Headsets" data-each="42.25" data-price="169.00" class="radio_button_common">
                    <div class="pack_type">
                        <label for="set4">Buy 4 VR Shinecon Headsets<br> ($42.25 Per Unit)</label>
                    </div>
                    <div class="price_box">
                        $169
                    </div>
                </div>
                <div class="pack_details">
                    <input type="radio" name="packSelect" id="set10" data-qty="10" data-pid="6" data-val="48" data-name="Buy 10 VR Shinecon Headsets" data-each="35.00" data-price="350.00" class="radio_button_common">
                    <div class="pack_type">
                        <label for="set10">Buy 10 VR Shinecon Headsets<br> ($35.00 Per Unit)</label>
                    </div>
                    <div class="price_box">
                        $350
                    </div>
                </div>
                <div class="pack_details">
                    <input type="radio" name="packSelect" id="set15" data-qty="15" data-pid="7" data-val="49" data-name="Buy 15 VR Shinecon Headsets" data-each="35.00" data-price="525.00" class="radio_button_common">
                    <div class="pack_type">
                        <label for="set15">Buy 15 VR Shinecon Headsets<br> ($35.00 Per Unit)</label>
                    </div>
                    <div class="price_box">
                        $525
                    </div>
                </div>
                <div class="pack_details">
                    <input type="radio" name="packSelect" id="set20" data-qty="20" data-pid="8" data-val="50" data-name="Buy 20 VR Shinecon Headsets" data-each="35.00" data-price="700.00" class="radio_button_common">
                    <div class="pack_type">
                        <label for="set20">Buy 20 VR Shinecon Headsets<br> ($35.00 Per Unit)</label>
                    </div>
                    <div class="price_box">
                        $700
                    </div>
                </div>
            </div>"""
import re
# print(468, log.get_error_text())
# result = re.findall(r'(<input[^>]+data-qty=\"([^"]+)\")', text)
# print(settings.PRODUCTS.values())
dic = {}
for product in settings.PRODUCTS.values():
    dic[product['id']] = product['name']
pprint(dic)

# print(text)
# cnt = Counter(list(settings.PRODUCTS.values()))
# cnt = Counter(dict(settings.PRODUCTS))
# pprint(list(dir(cnt)))
# print(cnt.)