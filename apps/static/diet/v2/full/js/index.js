$(document).ready(function() {

    $('a[href^="#"]').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
            && location.hostname == this.hostname) {
            var $target = $(this.hash);
            $target = $target.length && $target
            || $('[name=' + this.hash.slice(1) +']');
            if ($target.length) {
                var targetOffset = $target.offset().top;
                $('html,body')
                    .animate({scrollTop: targetOffset}, 1000);
                return false;
            }
        }
    });
    $('a.toTop').click(function(){
        $('input#first_name').focus();
    });



    //MASK
    $('#phone_mask').mask('(000) 000-0000');

    $('#phone_mask').bind('keyup keydown change', function(){
        $('#phone').val($('#phone_mask').cleanVal());
        $('#phone_mask').blur(function(){
            $('#phone').blur();
        });
    });

    if ($('#phone_mask').cleanVal()) {
        $('#phone_mask').change();
    }



    //ADDRESS
    var address = $('#address');
    var form = $('#state').closest('form');
    var masks = $('#city, #state, #zip').parent();

    form.addClass('mask_address');
    masks.addClass('address_field');
    address.attr('placeholder', '');

    if (address.val())
        form.removeClass('mask_address');

    address.bind('blur keydown', function() {
        form.removeClass('mask_address');;
    });

    initAutoComplete({
        fieldId: 'address',
        updateMap: {
            city: 'locality',
            state: 'administrative_area_level_1',
            zip: 'postal_code'
        }
    });
});