
$(document).ready(function() {


    var accepted = ['visa', 'master'];
    var classes = accepted.join(' ');

    $(function() {

        $('#card_number input').validateCreditCard(function(result) {

            // valid card number
            if (result.length_valid && result.luhn_valid == true) {
                $('#card_number').addClass('is-valid');
            }
            else {
                $('#card_number').removeClass('is-valid');
            }

            // no card type
            if (result.card_type == null) {
                $('#card_number').removeClass(classes);
                $('#card-error').html('');
                $('#card-type').val('visa');
            }

            // accepted card type
            else if (accepted.indexOf(result.card_type.name) !== -1) {
                $('#card_number').addClass(result.card_type.name);
                $('#card-type').val(result.card_type.name);
            }

            // unaccepted card type
            else {
                $('#card_number').removeClass(classes);
                $('#card-error').html('Invalid Card Type.');
                $('#card-type').val('visa');
            }
        });

    });




    $("input:checkbox[name=billing_same_as_shipping]").click(function() {
        var billing_fields = $('.billing_fields');
        if (!$(this).is(':checked') ){
            billing_fields.slideDown();
        } else {
            billing_fields.slideUp();
        }
    });


    $('#billing_city, #billing_state, #billing_zip').parent().addClass('address_field');
    initAutoComplete({
        fieldId: 'billing_street_address',
        updateMap: {
            billing_city: 'locality',
            billing_state: 'administrative_area_level_1',
            billing_postcode: 'postal_code'
        }
    });


    $('#card_number_mask').mask('0000 0000 0000 0000');

    $('#card_number_mask').bind('keyup keydown change', function(){
        $('#card_number').val($('#card_number_mask').cleanVal());
        $('#card_number_mask').blur(function(){
            $('#card_number').blur();
        });
    });

    if ($('#card_number_mask').cleanVal()) {
        $('#card_number_mask').change();
    }
});