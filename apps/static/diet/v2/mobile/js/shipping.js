$(document).ready(function(){
    $('#phone_mask').mask('(000) 000-0000');

	$('#phone_mask').bind('keyup keydown change', function(){
   		$('#phone').val($('#phone_mask').cleanVal());
		$('#phone_mask').blur(function(){
			$('#phone').blur();
		});
	});

	if ($('#phone_mask').cleanVal()) {
		$('#phone_mask').change();
	}
	$('#city, #state, #zip').parent().addClass('address_field');
	initAutoComplete({
		fieldId: 'address',
		updateMap: {
			city: 'locality',
			state: 'administrative_area_level_1',
			zip: 'postal_code'
		}
	});
});