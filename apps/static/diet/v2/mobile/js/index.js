$(document).ready(function(){
    $("#benefit_one .show").click(function () {
        $("#benefit_one .benefit_bottom").slideDown("slow");
    });
    $("#benefit_one .hide").click(function(){
        $("#benefit_one .benefit_bottom").slideUp("fast");
    });
    $("#benefit_two .show").click(function(){
        $("#benefit_two .benefit_bottom").slideDown("fast");
    });
    $("#benefit_two .hide").click(function(){
        $("#benefit_two .benefit_bottom").slideUp("slow");
    });
    $("#benefit_three .show").click(function(){
        $("#benefit_three .benefit_bottom").slideDown("fast");
    });
    $("#benefit_three .hide").click(function(){
        $("#benefit_three .benefit_bottom").slideUp("slow");
    });
    $('a[href^="#"]').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
            && location.hostname == this.hostname) {
            var $target = $(this.hash);
            $target = $target.length && $target
                || $('[name=' + this.hash.slice(1) +']');
            if ($target.length) {
                var targetOffset = $target.offset().top;
                $('html,body')
                    .animate({scrollTop: targetOffset}, 1000);
                return false;
            }
        }
    });
});
$(document).scroll(function() {
    var y = $(this).scrollTop();
    if (y > 3800){
        $('.index_footer').slideDown("fast");
    } else {
        $('.index_footer').slideUp("fast");
    }
});