# -*- coding: utf8 -*-
from __future__ import unicode_literals
print('urls, oleyshop')
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.flatpages.views import flatpage
from django.views.generic import TemplateView
from django.views import defaults as default_views
from django.contrib import flatpages

from apps.oleyshop import views


urlpatterns = [
    # url(r'^$', TemplateView.as_view(template_name='v2/index.html'), name='home'),
    url(r'^%s$' % settings.URL_FOLDER, views.HomeView.as_view(template_name='index.html'), name='home'),
    url(r'^%sabout/$' % settings.URL_FOLDER, views.MyTemplateView.as_view(template_name='about.html'), name='about'),
    url(r'^%sproducts/$' % settings.URL_FOLDER, views.MyTemplateView.as_view(template_name='products.html'), name='products'),
    url(r'^%squality/$' % settings.URL_FOLDER, views.MyTemplateView.as_view(template_name='quality.html'), name='quality'),
    url(r'^%scart/$' % settings.URL_FOLDER, views.CartPage.as_view(template_name='checkout.html'), name='cart'),
    url(r'^%scontact/$' % settings.URL_FOLDER, views.MyTemplateView.as_view(template_name='contact.html'), name='contact'),
    url(r'^%ssuccess/$' % settings.URL_FOLDER, views.SuccessView.as_view(template_name='success.html'), name='success'),
    url(r'^%scart/(?P<action>[\w.@+-]+)/$' % settings.URL_FOLDER, views.CartAction.as_view(), name='cart-action'),

                  #
    # url(r'^embed/$', EmbedView.as_view(template_name='embed.html'), name='embed')
    url(settings.ADMIN_URL, admin.site.urls),

                  # Your stuff: custom urls includes go here

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request, kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', default_views.server_error),
    ]
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns += [
            url(r'^__debug__/', include(debug_toolbar.urls)),
        ]
