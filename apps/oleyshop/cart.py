from apps.main.cart import Cart as BaseCart

class Cart(BaseCart):
    def add_to_cart(self, new_prod):
        new_prod = dict(new_prod)
        #if product in products - then i have to increase qty of product
        prod = self.find_product_in_products(new_prod)
        if prod:
            prod['qty'] += new_prod['qty']
            self.calc_product(prod)
        else:
            new_prod = self.calc_product(new_prod)
            self.products.append(new_prod)

        if self.request:
            self.request.session['order'] = self.products

    def sub_from_cart(self, sub_prod):
        """
        remove 1 item from cart
        """
        # if product in products - then i have to increase qty of product
        prod = self.find_product_in_products(sub_prod)
        if prod:
            prod['qty'] -= sub_prod['qty']
            if prod['qty'] <= 0:
                self.products.pop(self.products.index(prod))
            else:
                self.calc_product(prod)

        if self.request:
            self.request.session['order'] = self.products

    def calc_product(self, product):
        product['total'] = product.get('price', 0) * product.get('qty', 0) + product.get('shipping', 0)
        product['total_price'] = product.get('price', 0) * product.get('qty', 0)
        return product

    def find_product_in_products(self, product):
        pfound = None
        for i, prod in enumerate(self.products):
            if prod['id'] == product['id']:
                pfound = prod
                break
        return pfound

    def rm_from_cart(self, sub_prod):
        """
        remove item fully from cart
        """
        prod = self.find_product_in_products(sub_prod)
        if prod:
            self.products.pop(self.products.index(prod))

        if self.request:
            self.request.session['order'] = self.products
        return True