from pprint import pprint

from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic import View

from apps.limelight.models import Order
from apps.main.cart import Cart
from apps.main.views import MyTemplateView, OnlyFullMixin, SendOrderMixin, HasOrderMixin, SendUpsellMixin, \
    OnlyMobileMixin, SendOrderWithProspectMixin, HasProspectMixin, BaseHomeView, get_cart, SendFromCart, NoOrderMixin
from apps.main.views import SendProspectMixin

oleyshop_id = 235


class HomeView(BaseHomeView):
    pass


class CheckoutView(OnlyFullMixin, HasProspectMixin, MyTemplateView, SendOrderWithProspectMixin):
    next = 'upsell'
    back = 'home'
    mobile = 'payment'


class UpsellView(HasOrderMixin, SendUpsellMixin, MyTemplateView):
    next = 'upsell2'
    actions = {'yes': 'upsell_1',
               'yes3': 'upsell1_3',
               'yes5': 'upsell1_5'}
    back = 'order'

class Upsell2View(HasOrderMixin, SendUpsellMixin, MyTemplateView):
    next = 'success'
    actions = {'yes': 'upsell2_1',
               'yes3': 'upsell1_3',
               'yes5': 'upsell1_5'}
    back = 'order'


class SuccessView(HasOrderMixin, MyTemplateView):
    back = 'order'

    def clean_session(self):
        list_keys = ['main_order_id', 'prospect_task', 'order_task', 'order', 'repl']
        for key in list_keys:
            print(49, 'clean %s' % key)
            if key in self.request.session:
                self.request.session.pop(key)

    def get(self, request, *args, **kwargs):
        log = self.is_celery_error('order_task', request)
        if log is True:
            self.request.session.pop('order_task')
            return HttpResponseRedirect(reverse(self.back))

        if log and not request.session.get('main_order_id'):
            request.session['main_order_id'] = log.get_order_id()

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        cart = Cart(self.request)
        order_id = self.request.session.get('main_order_id', Order.objects.filter(limelight_id__isnull=False).last().limelight_id)
        print(69, len(cart.products))
        if len(cart.products) == 0 and self.request.user.is_staff:
            cart.add_to_cart(settings.PRODUCTS['1'])
            cart.add_to_cart(settings.PRODUCTS['protection'])
            cart.add_to_cart(settings.PRODUCTS['upsell_1'])
            cart.add_to_cart(settings.PRODUCTS['upsell2_1'])
            order_id = Order.objects.all().last().limelight_id
        context['cart'] = cart
        context['order_id'] = order_id
        # if 'main_product_id' in self.request.session:
        #     context['main_product'] = settings.PRODUCTS.get(self.request.session.get('main_product_id'))
        context['address'] = Order.objects.get(limelight_id=order_id).get_address_dict()
        return context

    def get_total(self, order):
        total = 0
        for line in order:
            if line and type(line) == type({}) and line.get('total'):
                total += float(line.get('total'))
        return round(total, 2)


class SelectView(OnlyMobileMixin, MyTemplateView):
    back = 'home'
    next = 'shipping'

    def add_product_to_cart(self, product_id):
        self.request.session['main_product_id'] = product_id
        product = settings.PRODUCTS.get(self.request.session['main_product_id'])
        product['total'] = product['price'] * product['qty']
        self.request.session['main_product'] = product
        return ""

    def get(self, request, *args, **kwargs):
        if 'product' in request.GET:
            self.add_product_to_cart(request.GET['product'])
            return HttpResponseRedirect(reverse(self.next))
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if 'product' in request.POST:
            self.add_product_to_cart(request.POST['product'])
            return HttpResponseRedirect(reverse(self.next))


class ShippingView(OnlyMobileMixin, MyTemplateView, SendProspectMixin):
    back = 'select'
    next = 'payment'


class PaymentView(OnlyMobileMixin, SendOrderWithProspectMixin, MyTemplateView):
    back = 'shipping'
    next = 'upsell'


class CartAction(View):
    def get(self, request, *args, **kwargs):
        action = kwargs.get('action')
        product = settings.PRODUCTS.get(request.GET.get('p'))
        cart = get_cart(request)
        # add
        if action == 'add':
            cart.add_to_cart(product)
        # sub
        elif action == 'sub':
            cart.sub_from_cart(product)
        # clear
        elif action == 'clear':
            cart.clear()
        elif action == 'rm':
            print('137, rm')
            cart.rm_from_cart(product)

        return HttpResponseRedirect(reverse('cart'))


class CartPage(NoOrderMixin, SendFromCart, MyTemplateView):
    next = 'success'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cart'] = get_cart(self.request)
        print(144, context['cart'])
        return context
