from pprint import pprint

from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.contrib.sites.models import Site
from django.db import models
from model_utils.models import TimeStampedModel
import json
import time


class CrmLogs(TimeStampedModel):
    crm_name = models.CharField(blank=True, max_length=30)
    command_type = models.CharField(null=True, blank=True, max_length=100)
    url = models.CharField(null=True, blank=True, max_length=255)
    # send_text = JSONField(null=True, blank=True)
    # get_text = JSONField(null=True, blank=True)
    send_text = models.TextField(null=True, blank=True)
    get_text = models.TextField(null=True, blank=True)
    task_id = models.CharField(max_length=255, blank=True, null=True)
    site = models.ForeignKey(Site, default=1)

    is_error = models.NullBooleanField()

    # is_real = models.BooleanField(default=True)

    def __str__(self):
        return u"%s - %s - %s" % (self.id, self.url, self.modified)

    def get_time(self):
        length = (self.modified - self.created)
        return "%s.%s sec" % (length.seconds, length.microseconds // 100000)

    def get_res(self):
        """
        return result_dict
        :return:
        """
        try:
            result = json.loads(self.get_text.replace("'", '"'))
        except:
            result = self.get_text
        return result

    def get_send(self):
        try:
            result = json.loads(self.send_text.replace("'", '"'))
        except:
            result = self.send_text
        return result

    def is_finish(self):
        # TODO make waiting finish 5-7 sec
        for i in range(7):
            log = CrmLogs.objects.get(id=self.id)
            print(i, log.get_text)
            if log.get_text is not None:
                self.get_text = log.get_text
                self.is_error = log.is_error
                return True
            time.sleep(1)

        self.is_error = True
        self.get_text = 'Too long response. Please try again later'
        self.save()
        return True

    def get_error_text(self):
        res = self.get_res()
        error_text = ''
        for name in ['declineReason', 'errorMessage']:
            if name in res and res.get(name):
                error_text = res.get(name)
        if not error_text:
            error_text = str(res)
        return error_text

    def get_order_id(self):
        res = self.get_res()
        return res.get('orderId')

    def get_prospect_id(self):
        res = self.get_res()
        return res.get('prospectId')

    def create_order(self, prospect_id=None):
        print(87)
        if self.command_type in ['NewOrder', 'NewOrderCardOnFile',
                                 'NewOrderWithProspect'] and self.is_finish() and not self.is_error:
            print(90, Order.objects.filter(log=self.id))
            if not Order.objects.filter(log=self.id).exists():
                print(92)
                res = self.get_res()
                send = self.get_send()

                test_card_number = '4640111111111111'
                name = "%s %s" % (send.get('firstName'), send.get('lastName'))
                if prospect_id:
                    prospect = Prospect.objects.get(limelight_id=prospect_id)
                    name = "%s %s" % (prospect.first_name, prospect.last_name)
                print(98, settings.PRODUCTS_FOR_ADMIN.get(send.get('productId')), send.get('productId'))
                base_order_dict = {
                    'name': name,
                    'prospect_id': prospect.id if prospect_id else None,
                    'product_name': settings.PRODUCTS_FOR_ADMIN.get(send.get('productId')),
                    'count': send.get("product_qty_%s" % send.get('productId'), 1),
                    'order_total': res.get('orderTotal'),
                    'limelight_id': res.get('orderId'),
                    'log_id': self.id,
                    'created': self.created,
                    'product_id': send.get('productId'),
                    'site_id': self.site_id,
                }
                order_dict = {}

                if self.command_type == 'NewOrder':
                    order_dict = base_order_dict
                    order_dict.update({
                        'is_real': True if send.get('creditCardNumber') != test_card_number else False,
                    })
                elif self.command_type == 'NewOrderCardOnFile':
                    order_id = send.get('previousOrderId')
                    print(94, order_id)
                    main_order_dict = Order.objects.filter(limelight_id=order_id).values('name', 'is_real')
                    if not main_order_dict:
                        return ''
                    order_dict = base_order_dict
                    order_dict.update(main_order_dict[0])
                elif self.command_type == 'NewOrderWithProspect':
                    order_dict = base_order_dict
                    order_dict.update({
                        'is_real': True if send.get('creditCardNumber') != test_card_number else False,
                    })
                pprint(order_dict)

                order = Order.objects.create(**order_dict)
                return order

    def save_prospect(self):
        if self.command_type in ['NewProspect'] and not self.is_error:
            send = self.get_send()
            prospect_id = self.get_prospect_id()
            if not Prospect.objects.filter(limelight_id=prospect_id).exists():
                prospect_dict = Prospect().prepare_dict_from_ll(send)
                prospect_dict['limelight_id'] = prospect_id
                prospect_dict['site_id'] = self.site_id
                Prospect.objects.create(**prospect_dict)


class Order(models.Model):
    name = models.CharField(blank=True, null=True, max_length=50)
    product_id = models.CharField(blank=True, null=True, max_length=30)
    product_name = models.CharField(blank=True, null=True, max_length=60)
    count = models.IntegerField(blank=True, null=True)
    created = models.DateTimeField(blank=True, null=True)
    order_total = models.FloatField(blank=True, null=True, max_length=30)
    limelight_id = models.IntegerField(blank=True, null=True)
    log = models.ForeignKey(CrmLogs, blank=True, null=True)
    is_real = models.BooleanField(default=True)
    prospect = models.ForeignKey('Prospect', blank=True, null=True, default=None)
    site = models.ForeignKey(Site, default=1)

    def __str__(self):
        return "%s - %s - %s" % (self.name, self.product_id, self.order_total)

    def get_ll_link(self):
        url = "https://oneclickawaytgc.limelightcrm.com/admin/orders.php?show_details=show_details&show_folder=" \
              "view_all&fromPost=1&act=&sequence=1&show_by_id=%s" % self.limelight_id
        return "<a href='%s' target='_blank'>%s</a>" % (url, self.limelight_id)

    get_ll_link.allow_tags = True
    get_ll_link.short_description = "LL order"

    def created_time(self):
        return "%s" % self.created.strftime("%Y-%m-%d %H:%M")

    def get_address_dict(self):
        if self.prospect:
            addr_dict = {
            'city': self.prospect.city,
            'address': self.prospect.address1
            }
        else:
            send_dict = self.log.get_send()
            addr_dict = {
                'city': send_dict.get('shippingCity'),
                'address': send_dict.get('shippingAddress1')
            }
        return addr_dict

    created_time.short_description = 'Created'


class Prospect(TimeStampedModel):
    """
    Even if user does not make purchase, we save his info fo further
    processing
    """
    first_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255, blank=True, null=True)
    address1 = models.CharField(max_length=255, blank=True, null=True)
    address2 = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    state = models.CharField(max_length=255, blank=True, null=True)
    zipcode = models.CharField(max_length=255, blank=True, null=True)
    country = models.CharField(max_length=255, default='US')
    phone = models.CharField(max_length=255, blank=True, null=True)
    user_agent = models.CharField(max_length=255, blank=True, null=True)
    email = models.EmailField(blank=True, null=True, max_length=96)
    ip = models.GenericIPAddressField()
    limelight_id = models.IntegerField(blank=True, null=True)
    task_id = models.CharField(max_length=255, blank=True, null=True)
    site = models.ForeignKey(Site, default=1)

    def __str__(self):
        return "%s, %s" % (self.first_name, self.last_name)

    def prepare_dict_from_ll(self, ll_dict):
        prospect = {
            'first_name': ll_dict.get('firstName'),
            'last_name': ll_dict.get('lastName'),
            'address1': ll_dict.get('address1'),
            'address2': ll_dict.get('address2'),
            'city': ll_dict.get('city'),
            'state': ll_dict.get('state'),
            'zipcode': ll_dict.get('zip'),
            'country': ll_dict.get('country'),
            'phone': ll_dict.get('phone'),
            'user_agent': ll_dict.get('', ''),
            'email': ll_dict.get('email'),
            'ip': ll_dict.get('ipAddress'),
            'limelight_id': ll_dict.get(''),
        }
        return prospect
