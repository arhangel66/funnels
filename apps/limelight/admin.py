from django import forms
from django.contrib import admin
# from prettyjson import PrettyJSONWidget
from apps.limelight.models import CrmLogs, Order, Prospect


# class JsonForm(forms.ModelForm):
#     class Meta:
#         model = CrmLogs
#         fields = '__all__'
#         widgets = {
#             'get_text': PrettyJSONWidget(),
#             'send_text': PrettyJSONWidget(),
#         }


@admin.register(CrmLogs)
class CrmLogsAdmin(admin.ModelAdmin):
    list_display = ('id', 'site', 'command_type', 'is_error', 'created', 'get_time')
    list_filter = ('url', 'site', 'command_type', 'is_error',)

    # form = JsonForm


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = (
    'id', 'site',  'name', 'product_id', 'product_name', 'count', 'order_total', 'is_real', 'get_ll_link', 'created_time')
    list_filter = ('is_real', 'site',)

    # form = JsonForm


@admin.register(Prospect)
class ProspectAdmin(admin.ModelAdmin):
    list_display = ('id', 'site', 'first_name', 'last_name', 'phone',)
    list_filter = ('site',)

    # form = JsonForm
