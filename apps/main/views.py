import re
from pprint import pprint

from django.conf import settings
from django.contrib.auth.mixins import AccessMixin
from django.contrib.sites.shortcuts import get_current_site
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import Template, Context, RequestContext
from django.template.loader import render_to_string
from django.views.generic import TemplateView, FormView, View
from django.contrib import messages
from apps.limelight.api import LimeLightApi, get_client_ip
from apps.limelight.card_type import get_card_type
from apps.limelight.models import CrmLogs, Prospect, Order
from django.http import HttpResponse
from apps.limelight.tasks import call
from apps.main.cart import Cart
from apps.main.models import SnippetGlobal, SnippetPage, SnippetSite

diet_id = 235


def add_order_to_cart(request, product):
    pass


def is_log_error(log_id, request):
    log = CrmLogs.objects.get(id=log_id)
    if log.is_error:
        # messages.add_message(request, messages.INFO, log.get_error_text())
        if request:
            messages.add_message(request, messages.INFO, log.get_error_text())
        return True
    return log


from pydoc import locate



def get_cart(request=None):
    cart = Cart(request)
    if hasattr(settings, 'CART_MODEL'):
        cart = locate(settings.CART_MODEL)(request)
    return cart


class OnlyMobileMixin(AccessMixin):
    """
    if it is not mobile will redirect to back page
    """
    back = 'home'

    def dispatch(self, request, *args, **kwargs):
        if request.flavour != 'mobile':
            return HttpResponseRedirect(reverse(self.back))

        return super().dispatch(request, *args, **kwargs)


class OnlyFullMixin(AccessMixin):
    """
    if it is not mobile will redirect to back page
    """
    mobile = 'payment'

    def dispatch(self, request, *args, **kwargs):
        if request.flavour == 'mobile':
            return HttpResponseRedirect(reverse(self.mobile))

        return super().dispatch(request, *args, **kwargs)


class HasOrderMixin(AccessMixin):
    """
    View mixin which verifies that the user is authenticated.
    """
    back = 'home'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            if not (request.session.get('main_order_id') or request.session.get('order_task')):
                return HttpResponseRedirect(reverse(self.back))

        return super().dispatch(request, *args, **kwargs)


class NoOrderMixin(AccessMixin):
    """
    View mixin which verifies that the user is authenticated.
    """
    next = 'success'

    def dispatch(self, request, *args, **kwargs):
        if (request.session.get('main_order_id') or request.session.get('order_task')):
            return HttpResponseRedirect(reverse(self.next))

        return super().dispatch(request, *args, **kwargs)

class HasProspectMixin(AccessMixin):
    """
    View mixin which verifies that the user is authenticated.
    """
    back = 'home'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            if not (request.session.get('prospect_task')):
                return HttpResponseRedirect(reverse(self.back))

        return super().dispatch(request, *args, **kwargs)


class AbstractBaseView(object):
    def is_celery_error(self, celery_task_id, request=None):
        if request.session.get(celery_task_id) is None:
            return False
        log_id = call.AsyncResult(request.session[celery_task_id]).get()
        log = CrmLogs.objects.get(id=log_id)
        if log.is_error:
            # messages.add_message(request, messages.INFO, log.get_error_text())
            if request:
                messages.add_message(request, messages.INFO, log.get_error_text())
            return True
        return log


class MyTemplateView(TemplateView, AbstractBaseView):
    def get(self, request, *args, **kwargs):
        print(89, self.get_template_names())
        context = self.get_context_data(**kwargs)
        # context['request'] = request
        context['test'] = 'test222'
        # messages.add_message(request, messages.INFO, 'test41')

        html = render_to_string(self.get_template_names()[0], context,
                                context_instance=RequestContext(request))

        print(74, request.flavour)

        html = self.process_snippets(html, page=request.path, site=get_current_site(request))
        html = self.change_page_urls(html)
        html = self.change_static(html, flavour=request.flavour)
        html = self.set_csrf_token(html)
        # print(23, html)


        t = Template(html)

        html = t.render(RequestContext(request, context))
        # print(27, html)
        return HttpResponse(html)

    def change_static(self, html, flavour=None):
        path = settings.FULL_STATIC_URL
        if flavour == 'mobile':
            path = settings.MOBILE_STATIC_URL

        html = re.sub(r'(src="|href=")([^"]+\.html"|js|img|css|images|fonts)', '\\1%s\\2' % path, html)
        html = re.sub(r'(openNewWindow\(\&\#39\;)([^&]+\.html\&\#39\;)', '\\1/%s\\2' % path, html)
        html = re.sub(r'(<input[^>]+data-qty=\")([^"]+)(\")', '\\1\\2" value="\\2\\3', html)  # add val to select inputs
        html = html.replace('//static', '/static')
        return html

    def set_csrf_token(self, html):
        html = re.sub(r'(<form[^>]+>)', '\\1 \n {% csrf_token %}', html)
        return html

    def process_snippets(self, html, page, site):
        """
        Add all snippets to html page.
        Returns new updated html page.
        """
        dic = {}
        for s in SnippetGlobal.objects.all():
            dic[s.name] = s

        for s in SnippetSite.objects.filter(site=site):
            dic[s.name] = s

        for s in SnippetPage.objects.filter(page=page, site=site):
            dic[s.name] = s

        for name, s in dic.items():
            html = s.add_snippet(html)

        return html

    def change_page_urls(self, html):
        if hasattr(settings, 'PAGES'):
            pages = settings.PAGES
            for template_name, page_name in pages.items():
                if template_name in html:
                    try:
                        html = html.replace(template_name, reverse(page_name))
                    except:
                        print('no %s in urls' % page_name)
        return html

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['products'] = settings.PRODUCTS

        return context


class AbstractSendMixin(object):
    next = '/'
    prefix = ''
    products_base = settings.PRODUCTS

    def get_product(self, product_name):
        return self.products_base.get(product_name)

    def use_replenish(self, order_data=None, request=None):
        """if user choose replenish - then use other products base"""
        print('166, will we use replenish products? - %s' % (order_data and order_data.get('continuity')) or (request and request.session.get('repl')))
        if (order_data and order_data.get('continuity')) or (request and request.session.get('repl')):
            self.products_base = settings.PRODUCTS_REPL
            if request:
                request.session['repl'] = True
        else:
            self.products_base = settings.PRODUCTS

    def redirect_to_next(self):
        if not '/' in self.next:
            return HttpResponseRedirect(reverse(self.next))
        return HttpResponseRedirect(self.next)

    def apply_prefix(self, url):
        return "%s%s" % (self.prefix, url)

    def get_next(self):
        if '/' in self.next:
            next = self.next
        else:
            try:
                next = reverse(self.next)
            except:
                next = self.next
        return self.apply_prefix(next)

    def get_back(self):
        if '/' in self.back:
            back = self.back
        else:
            try:
                back = reverse(self.back)
            except:
                back = self.back
        return self.apply_prefix(back)

    def get_this_url(self):
        if '/' in self.this:
            this = self.this
        else:
            try:
                this = reverse(self.this)
            except:
                this = self.this
        # print(105, this, self.apply_prefix(this))
        return self.apply_prefix(this)

class SendProspectMixin(AbstractSendMixin):
    def post(self, request, *args, **kwargs):
        api = LimeLightApi(site_id=get_current_site(request))
        prospect_data = {}
        for key in request.POST.keys():
            prospect_data[key] = request.POST[key]
        prospect_data['ipAddress'] = get_client_ip(request)
        if 'shippingAddress1' in prospect_data:
            prospect_data['address1'] = prospect_data.get('shippingAddress1')
        if 'shippingCountry' in prospect_data:
            prospect_data['country'] = prospect_data.get('shippingCountry')
        if 'shippingState' in prospect_data:
            prospect_data['state'] = prospect_data.get('shippingState')
        if 'shippingCity' in prospect_data:
            prospect_data['city'] = prospect_data.get('shippingCity')
        if 'shippingZip' in prospect_data:
            prospect_data['zip'] = prospect_data.get('shippingZip')

        request.session['prospect_task'] = api.send_prospect(prospect_data).id
        print(146, request.session['prospect_task'])
        # create prospect in DB
        # prospect_dict_for_db = Prospect().prepare_dict_from_ll(prospect_data)
        # prospect_dict_for_db['task_id'] = request.session['prospect_task']
        # prospect = Prospect.objects.create(**prospect_dict_for_db)
        if not '/' in self.next:
            return HttpResponseRedirect(reverse(self.next))
        return HttpResponseRedirect(self.next)


class SendOrderWithProspectMixin(AbstractSendMixin):
    product = '5'
    next = '/'

    def post(self, request, *args, **kwargs):
        log = self.is_celery_error('prospect_task', request)
        if log is True:
            return HttpResponseRedirect(reverse(self.back))
        log.save_prospect()

        if self.request.session.get('main_product_id'):
            self.product = self.request.session.get('main_product_id')

        prospect_id = log.get_prospect_id()
        request.session['prospect_id'] = prospect_id

        # Prospect.objects.filter(task_id=request.session['prospect_task']).update(limelight_id=prospect_id)
        order_data = {}
        for key in request.POST.keys():
            order_data[key] = request.POST[key]

        if not order_data.get('creditCardType'):
            order_data['creditCardType'] = get_card_type(order_data.get('creditCardNumber', None))
        order_data['expirationDate'] = "%s%s" % (order_data.get('expmonth'), order_data.get('expyear'))
        order_data['tranType'] = 'Sale'


        if 'packSelect' in order_data:
            self.product = order_data['packSelect']
            self.request.session['main_product_id'] = self.product


        self.use_replenish(order_data, request)
        product = self.get_product(self.product)
        order_data = self.add_product_to_order_data(order_data, product)

        api = LimeLightApi(site_id=get_current_site(request))

        request.session['order_task'] = api.send_order_with_prospect(order_data, prospect_id).id
        cart = get_cart(request)
        cart.clear()
        cart.add_to_cart(product)

        if not 'decline_protection' in order_data:
            product = self.get_product('protection')
            order_data = self.add_product_to_order_data(order_data, product)
            request.session['protection_task'] = api.send_order_with_prospect(order_data, prospect_id).id
            cart.add_to_cart(product)

        if '/' not in self.next:
            return HttpResponseRedirect(reverse(self.next))
        return HttpResponseRedirect(self.next)

    def add_product_to_order_data(self, order_data, product):
        if product and 'id' in product:
            order_data['productId'] = product['id']
            if 'price' in product:
                order_data['dynamic_product_price_' + str(product['id'])] = product['price']
            if 'qty' in product and product['qty'] != 1:
                order_data['product_qty_%s' % product['id']] = product['qty']
        return order_data


class SendOrderMixin(AbstractSendMixin):
    product = ''
    next = '/'

    def post(self, request, *args, **kwargs):

        # Prospect.objects.filter(task_id=request.session['prospect_task']).update(limelight_id=prospect_id)
        order_data = {}
        for key in request.POST.keys():
            order_data[key] = request.POST[key]
        order_data['expirationDate'] = "%s%s" % (order_data.get('expmonth'), order_data.get('expyear'))
        order_data['tranType'] = 'Sale'

        pprint(order_data)
        self.use_replenish(order_data, request)
        product = self.get_product(order_data['packSelect'])
        print(184, product)
        if product and 'id' in product:
            order_data['productId'] = product['id']
            if 'price' in product:
                order_data['dynamic_product_price_' + str(product['id'])] = product['price']
            if 'qty' in product and product['qty'] != 1:
                order_data['product_qty_%s' % product['id']] = product['qty']

        order_data['ipAddress'] = get_client_ip(request)
        order_data['creditCardType'] = get_card_type(order_data.get('creditCardNumber', None))
        api = LimeLightApi(site_id=get_current_site(request))

        request.session['order_task'] = api.send_order_from_dict(order_data, ip=get_client_ip(request)).id
        cart = get_cart(request)
        cart.clear()
        cart.add_to_cart(product)
        if not '/' in self.next:
            return HttpResponseRedirect(reverse(self.next))
        return HttpResponseRedirect(self.next)


class SendFromCart(AbstractSendMixin):
    next = '/'
    this = 'cart'

    def post(self, request, *args, **kwargs):
        method = 'NewOrder'


        # get cart
        cart = get_cart(request)

        # if products in cart check (if prospect or in request or prospect fields)
        if len(cart.products) < 1:
            return False

        # get api
        api = LimeLightApi(request=request)

        if 'prospect_task' in request.session:
            # i have to get prospect_id and add to session id
            pass

        if 'prospect_id' in request.session:
            pass
            method = 'NewOrderWithProspect'


        for i, product in enumerate(cart.products):
            if 'main_order_id' in request.session:
                method = 'NewOrderCardOnFile'

            # default order_dict
            order_dict = api.get_default_dict(method)
            product_dict = api.get_product_dict(product)
            order_dict.update(product_dict)
            order_dict.update(api.get_extra_params())

            if i == 0:
                log_id = api.call(order_dict)
                log = is_log_error(log_id, request)
                if log is True:
                    return HttpResponseRedirect(self.get_this_url())
                request.session['main_order_id'] = log.get_order_id()
            else:
                api.call(order_dict, async=True)

            # mark product in cart as 'sended'

        return self.redirect_to_next()
        # if prospect - send add prospect_id to post and send
        # get default dict, update it

        # if product, then get dict_from product and update (qty, delivery, campaign_id)
        # send)


        # append AFID and other spec




class SendUpsellMixin(AbstractSendMixin):
    actions = {}
    next = '/'

    def get(self, request, *args, **kwargs):
        key = request.GET.get('action')
        if key:
            product_key = self.actions.get(key)
            if product_key is None:
                if not '/' in self.next:
                    return HttpResponseRedirect(reverse(self.next))
                return HttpResponseRedirect(self.next)

            self.use_replenish(request=request)
            product = self.get_product(product_key)

            # messages.add_message(request, messages.INFO, 'test132')
            log = self.is_celery_error('order_task', request)
            if log is True:
                return HttpResponseRedirect(reverse(self.back))

            request.session['main_order_id'] = log.get_order_id()

            api = LimeLightApi(site_id=get_current_site(request))
            api.new_order_card_on_file_async(request.session['main_order_id'], product['id'], price=product['price'],
                                             prospect_id=request.session.get('prospect_id'), qty=product['qty'])
            cart = get_cart(request)
            cart.add_to_cart(product)
            if not '/' in self.next:
                return HttpResponseRedirect(reverse(self.next))
            return HttpResponseRedirect(self.next)
        return super().get(request, *args, **kwargs)


class BaseHomeView(MyTemplateView):
    next = '/checkout/'

    def get(self, request, *args, **kwargs):
        request.session.delete()
        if request.method == 'GET' and 'AFID' in request.GET:
            request.session['AFID'] = request.GET['AFID']
        else:
            request.session['AFID'] = settings.SITE_NAME if hasattr(settings, 'SITE_NAME') else request.get_host()

        if request.method == 'GET' and 'SID' in request.GET:
            request.session['SID'] = request.GET['SID']

        if request.method == 'GET' and 'AFFID' in request.GET:
            request.session['AFFID'] = request.GET['AFFID']

        if 'custom' in request.GET:
            request.session['OPT'] = request.GET['custom']
        return super().get(request, *args, **kwargs)