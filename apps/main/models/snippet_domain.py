from django.contrib.sites.models import Site
from django.db import models
from django.contrib import admin
from .snippet_base import SnippetBase


class SnippetSiteManager(models.Manager):
    def create_snippet(self, name, value, type):
        s = self.model(name=name, value=value, type=type)
        s.save()
        return s


class SnippetSite(SnippetBase):
    objects = SnippetSiteManager()
    site = models.ForeignKey(Site, default=1)

    class Meta:
        verbose_name = 'snippet: site'
        verbose_name_plural = 'snippets: site'
        ordering = ['name']

    def __str__(self):
        return self.name


@admin.register(SnippetSite)
class SnippetSiteAdmin(admin.ModelAdmin):
    list_display = ('name', 'site', 'type', 'value')
    list_editable = ('value', 'type')
    list_filter = ('site', )