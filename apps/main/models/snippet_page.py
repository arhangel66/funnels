from django.contrib.sites.models import Site
from django.db import models
from django.contrib import admin
from .snippet_base import SnippetBase


class SnippetGlobalManager(models.Manager):
    def create_snippet(self, name, value, type):
        s = self.model(name=name, value=value, type=type)
        s.save()
        return s


class SnippetPage(SnippetBase):
    objects = SnippetGlobalManager()
    page = models.CharField(max_length=100, default='/', blank=True)
    site = models.ForeignKey(Site, default=1)

    class Meta:
        verbose_name = 'snippet: page'
        verbose_name_plural = 'snippets: page'
        ordering = ['name']

    def __str__(self):
        return self.name


@admin.register(SnippetPage)
class SnippetGlobalAdmin(admin.ModelAdmin):
    list_display = ('name', 'site', 'page', 'type', 'value')
    list_filter = ('page', 'site',)
    list_editable = ('value', 'type')
