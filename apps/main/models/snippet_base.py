from django.db import models


class SnippetBase(models.Model):
    HEAD_START = 'head_start'
    HEAD_END = 'head_end'
    BODY_START = 'body_start'
    BODY_END = 'body_end'
    HTML_END = 'html_end'

    TYPE_CHOICES = (
        (HEAD_START, 'head start'),
        (HEAD_END, 'head end'),
        (BODY_START, 'body start'),
        (BODY_END, 'body end'),
        (HTML_END, 'html end')
    )

    name = models.CharField(max_length=100)
    value = models.TextField(default='', blank=True)
    type = models.CharField(
        max_length=15, default=HEAD_START, choices=TYPE_CHOICES)

    class Meta:
        abstract = True

    def add_snippet(self, page):
        """
        Adds snippet to page and returns new updated page.
        """
        tag_name = self.type.replace('_start', '').replace('_end', '')
        is_start = True if self.type.endswith('start') else False
        if is_start:
            tag = '<{}>'.format(tag_name)
            new_value = tag + '\n' + self.value
        else:
            tag = '</{}>'.format(tag_name)
            new_value = self.value + '\n' + tag

        return page.replace(tag, new_value)
