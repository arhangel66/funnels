# -*- coding: utf8 -*-
__author__ = 'Derbichev Mikhail, arhangel662@gmail.com'
from oauth2client.service_account import ServiceAccountCredentials
from django.conf import settings
# The scope for the OAuth2 request.
SCOPE = 'https://www.googleapis.com/auth/analytics.readonly'

# The location of the key file with the key data.



# Defines a method to get an access token from the ServiceAccount object.
def get_access_token():
    return ServiceAccountCredentials.from_json_keyfile_name(
        settings.KEY_FILEPATH, SCOPE).get_access_token().access_token
