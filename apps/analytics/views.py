# -*- coding: utf8 -*-
from apps.analytics.access_token import get_access_token

__author__ = 'Derbichev Mikhail, arhangel662@gmail.com'
from django.views.generic import TemplateView


class EmbedView(TemplateView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['access_token'] = get_access_token()
        return context