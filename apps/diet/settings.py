# -*- coding: utf8 -*-
__author__ = 'Derbichev Mikhail, arhangel662@gmail.com'
import socket
import os
from config.settings.production import *

SITE_ID = 1
ROOT_URLCONF = 'apps.diet.urls'

version = 'v1'
FULL_STATIC_URL = '/static/diet/%s/full/' % version
MOBILE_STATIC_URL = '/static/diet/%s/mobile/' % version

# FLAVOURS_TEMPLATE_PREFIX = '../'

TEMPLATES[0]['DIRS'].append(str(APPS_DIR.path('static', 'diet', version)))
TEMPLATES[0]['DIRS'].append(str(APPS_DIR.path('static', 'diet', version, 'full')))

PRODUCTS_REPL = {
    '6': {'shipping': 0, 'id': '284', 'name': '6 BOTTLE MONSTER PACKAGE', 'price': 179, 'qty': 1, 'image': 'images/six_bottles.png'},
    '5': {'shipping': 0, 'id': '283', 'name': '5 BOTTLE MONSTER PACKAGE', 'price': 148, 'qty': 1, 'image': 'images/five_bottles.png'},
    '3': {'shipping': 0, 'id': '282', 'name': '3 BOTTLE MONSTER PACKAGE', 'price': 98, 'qty': 1, 'image': 'images/three_bottles.png'},
    '1': {'shipping': 0, 'id': '280', 'name': '1 BOTTLE STARTER PACKAGE', 'price': 48, 'qty': 1, 'image': 'images/garcinia_cambogia.png'},
    '0': {'shipping': 4.95, 'id': '269', 'name': 'Total Garcinia Cambogia - 30 Day Supply', 'price': 0, 'qty': 1, 'image': 'images/product_three.jpg'},
    'upsell_1': {'shipping': 0, 'id': '285', 'name': 'Total Cleanse 1', 'price': 29.95, 'qty': 1, 'image': 'images/garcinia_cleanse.png'},
    'upsell_5': {'shipping': 0, 'id': '286', 'name': 'Total Cleanse 5', 'price': 124.95, 'qty': 1, 'image': 'images/total_cleanse_six_bottles.png'},
    'upsell2_1': {'shipping': 0, 'id': '287', 'name': 'Slim Path 1', 'image': 'images/slims.png', 'price': 29.95, 'qty': 1},
    'upsell2_5': {'shipping': 0, 'id': '288', 'name': 'Slim Path 5', 'image': 'images/slims_five.png', 'price': 124.95, 'qty': 1},
    'protection': {'shipping': 1.99, 'name': 'Shipping protection', 'id': '276'}
}

PRODUCTS = {
    '6': {'shipping': 0, 'id': '60', 'name': '6 BOTTLE MONSTER PACKAGE nr', 'price': 179, 'qty': 1, 'image': 'images/six_bottles.png'},
    '5': {'shipping': 0, 'id': '5', 'name': '5 BOTTLE MONSTER PACKAGE nr', 'price': 148, 'qty': 1, 'image': 'images/five_bottles.png'},
    '3': {'shipping': 0, 'id': '3', 'name': '3 BOTTLE MONSTER PACKAGE nr', 'price': 98, 'qty': 1, 'image': 'images/three_bottles.png'},
    '1': {'shipping': 0, 'id': '1', 'name': '1 BOTTLE STARTER PACKAGE nr', 'price': 48, 'qty': 1, 'image': 'images/garcinia_cambogia.png'},
    '0': {'shipping': 4.95, 'id': '269', 'name': 'Total Garcinia Cambogia - 30 Day Supply', 'price': 0, 'qty': 1, 'image': 'images/product_three.jpg'},
    'upsell_1': {'shipping': 0, 'id': '25', 'name': 'Total Cleanse 1 nr', 'price': 29.95, 'qty': 1, 'image': 'images/garcinia_cleanse.png'},
    'upsell_5': {'shipping': 0, 'id': '33', 'name': 'Total Cleanse 5 nr', 'price': 124.95, 'qty': 1, 'image': 'images/total_cleanse_six_bottles.png'},
    'upsell2_1': {'shipping': 0, 'id': '68', 'name': 'Slim Path 1 nr', 'image': 'images/slims.png', 'price': 29.95, 'qty': 1},
    'upsell2_5': {'shipping': 0, 'id': '62', 'name': 'Slim Path 5 nr', 'image': 'images/slims_five.png', 'price': 124.95, 'qty': 1},
    'protection': {'shipping': 1.99, 'name': 'Shipping protection', 'id': '276'}
}

PRODUCTS_FOR_ADMIN = {
    '269': 'Total Garcinia Cambogia - 30 Day Supply',
    '276': 'Shipping protection',
    '280': '1 BOTTLE STARTER PACKAGE',
    '282': '3 BOTTLE MONSTER PACKAGE',
    '283': '5 BOTTLE MONSTER PACKAGE',
    '284': '6 BOTTLE MONSTER PACKAGE',
    '285': 'Total Cleanse 1',
    '286': 'Total Cleanse 5',
    '287': 'Slim Path 1',
    '288': 'Slim Path 5',
    '1': '1 BOTTLE STARTER PACKAGE nr',
    '3': '3 BOTTLE MONSTER PACKAGE nr',
    '5': '5 BOTTLE MONSTER PACKAGE nr',
    '60': '6 BOTTLE MONSTER PACKAGE nr',
    '25': 'Total Cleanse 1 nr',
    '33': 'Total Cleanse 5 nr',
    '68': 'Slim Path 1 nr',
    '62': 'Slim Path 5 nr'
}
LL.update({
    'campaign': '162',
    'shipping': '1'
})

BROKER_URL = 'redis://localhost:6379/12'

OPBEAT = {
    'ORGANIZATION_ID': 'c5674ae31fbc41529aadeb197db5fc85',
    'APP_ID': '4a57bc0070',
    'SECRET_TOKEN': '74a82295c93dcedeb04db5d0e0ed34204dc8fd7e',
}
