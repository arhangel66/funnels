# -*- coding: utf8 -*-
__author__ = 'Derbichev Mikhail, arhangel662@gmail.com'
from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect

from apps.limelight.models import Order
from apps.main.views import MyTemplateView, OnlyFullMixin, SendOrderMixin, HasOrderMixin, SendUpsellMixin, \
    OnlyMobileMixin, SendOrderWithProspectMixin
from apps.main.views import SendProspectMixin

diet_id = 235


class HomeView(MyTemplateView, SendProspectMixin):
    next = '/checkout/'

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class CheckoutView(OnlyFullMixin, MyTemplateView, SendOrderMixin):
    next = '/upsell/'
    product = 'diet'
    back = 'home'
    mobile = 'payment'

class UpsellView(HasOrderMixin, SendUpsellMixin, MyTemplateView):
    next = '/upsell2/'
    actions = {'yes1': 'upsell1_1',
               'yes3': 'upsell1_3',
               'yes5': 'upsell1_5'}
    back = 'checkout'


class Upsell2View(HasOrderMixin, SendUpsellMixin, MyTemplateView):
    next = '/success/'
    actions = {'yes1': 'upsell2_1',
               'yes3': 'upsell2_3',
               'yes5': 'upsell2_5'}

    back = 'checkout'


class SuccessView(HasOrderMixin, MyTemplateView):
    back = 'checkout'

    def clean_session(self):
        list_keys = [] #['main_order_id', 'prospect_task', 'order_task']
        for key in list_keys:
            if key in self.request.session:
                self.request.session.pop(key)

    def get(self, request, *args, **kwargs):
        log = self.is_celery_error('order_task', request)
        if log is True:
            return HttpResponseRedirect(reverse(self.back))
        print(201)
        if not request.session.get('main_order_id'):
            print(202, log, log.get_order_id())
            request.session['main_order_id'] = log.get_order_id()

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['total'] = self.get_total(self.request.session.get('order', []))
        context['subtotal'] = context['total'] - self.request.session.get('order', [])[0]['total']
        context['order'] = self.request.session.get('order')
        context['order_id'] = self.request.session['main_order_id']
        context['address'] = Order.objects.get(limelight_id=self.request.session['main_order_id']).get_address_dict()
        # self.request.session
        self.clean_session()
        return context

    def get_total(self, order):
        total = 0
        for line in order:
            if line and type(line) == type({}) and line.get('total'):
                total += float(line.get('total'))
        return round(total, 2)


class SelectView(OnlyMobileMixin, MyTemplateView):
    back = 'home'
    next = 'shipping'

    def post(self, request, *args, **kwargs):
        if 'packSelect'in request.POST:
            request.session['main_product_id'] = request.POST['packSelect']
            product = settings.PRODUCTS.get(request.session['main_product_id'])
            product['total'] = product['price'] * product['qty']
            request.session['main_product'] = product
            return HttpResponseRedirect(reverse(self.next))


class ShippingView(OnlyMobileMixin, MyTemplateView, SendProspectMixin):
    back = 'select'
    next = 'payment'


class PaymentView(OnlyMobileMixin, SendOrderWithProspectMixin, MyTemplateView):
    back = 'shipping'
    next = 'upsell'